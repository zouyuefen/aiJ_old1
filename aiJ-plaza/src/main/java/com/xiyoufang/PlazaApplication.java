package com.xiyoufang;

import com.xiyoufang.common.ApplicationContextProvider;
import com.xiyoufang.plaza.event.StartedEvent;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * 启动广场
 */
@SpringBootApplication
public class PlazaApplication {

    /**
     * @param args 参数
     */
    public static void main(String[] args) {
        new SpringApplicationBuilder(PlazaApplication.class).headless(false).web(WebApplicationType.NONE).run(args);
        ApplicationContextProvider.publishEvent(new StartedEvent(""));
    }

}
