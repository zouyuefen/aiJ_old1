package com.xiyoufang.plaza.ui.event;

import com.xiyoufang.plaza.ui.model.GameServerModel;
import org.springframework.context.ApplicationEvent;

/**
 * Created by 席有芳 on 2018/7/8.
 * 游戏服务器更新事件
 *
 * @author 席有芳
 */
public class GameServerUpdateEvent extends ApplicationEvent {

    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public GameServerUpdateEvent(GameServerModel source) {
        super(source);
    }
}
