package com.xiyoufang.plaza.ui.event;

import org.springframework.context.ApplicationEvent;

/**
 * Created by 席有芳 on 2018/7/8.
 * 广场服务器信息更新事件
 *
 * @author 席有芳
 */
public class PlazaInfoUpdateEvent extends ApplicationEvent {
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public PlazaInfoUpdateEvent(Object source) {
        super(source);
    }
}
