package com.xiyoufang.plaza.ui.event;

import com.xiyoufang.plaza.ui.model.RunLogModel;
import org.springframework.context.ApplicationEvent;

/**
 * Created by 席有芳 on 2018/7/8.
 * 添加日志事件
 *
 * @author 席有芳
 */
public class RunLogAddEvent extends ApplicationEvent {

    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public RunLogAddEvent(RunLogModel source) {
        super(source);
    }
}
