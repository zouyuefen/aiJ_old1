package com.xiyoufang.plaza.ui;

import com.xiyoufang.plaza.ui.event.GameServerUpdateEvent;
import com.xiyoufang.plaza.ui.event.PlazaInfoUpdateEvent;
import com.xiyoufang.plaza.ui.event.RunLogAddEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by 席有芳 on 2018/7/8.
 *
 * @author 席有芳
 */
@Component
public class MainUI {
    //开始服务按钮
    private JButton buttonStartServer;
    //停止服务按钮
    private JButton buttonStopServer;
    //关闭按钮
    private JButton buttonCloseServer;
    //服务器信息
    private JLabel labelServerInfo;
    //主面板
    private JPanel panelMain;
    //游戏服务器滚动
    private JScrollPane scrollPaneTableGameServer;
    //游戏日志
    private JScrollPane scrollPaneTableRunLog;
    //游戏服务器列表
    private JTable tableGameServer;
    //运行日志
    private JTable tableRunLog;

    //服务器列表
    private final static String GAME_SERVER_COLUMN_NAMES[] = {"IP地址", "IP端口", "运行状态", "同步时间", "服务器信息"};
    //运行日志
    private final static String RUN_LOG_COLUMN_NAMES[] = {"时间", "信息"};

    public MainUI() {

        //关闭进程
        buttonCloseServer.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             *  事件
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                exitApplication();
            }
        });
        buttonStartServer.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             *  事件
             */
            @Override
            public void actionPerformed(ActionEvent e) {


            }
        });
        buttonStopServer.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             * 事件
             */
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        initUIJTable(); //初始化界面删的表格
    }

    private void initUIJTable() {
        //游戏服务器表格
        tableGameServer = new JTable();
        tableGameServer.setFillsViewportHeight(true);
        tableGameServer.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableGameServer.setModel(new DefaultTableModel());

        //日志表格
        tableRunLog = new JTable();
        tableRunLog.setFillsViewportHeight(true);
        tableRunLog.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableRunLog.setModel(new DefaultTableModel());

        //设置表格头
        ((DefaultTableModel) tableGameServer.getModel()).setColumnIdentifiers(GAME_SERVER_COLUMN_NAMES);
        tableGameServer.getColumnModel().getColumn(0).setPreferredWidth(80);
        tableGameServer.getColumnModel().getColumn(1).setPreferredWidth(60);
        tableGameServer.getColumnModel().getColumn(2).setPreferredWidth(60);
        tableGameServer.getColumnModel().getColumn(3).setPreferredWidth(160);
        tableGameServer.getColumnModel().getColumn(4).setPreferredWidth(240);
        scrollPaneTableGameServer.setViewportView(tableGameServer);

        //日志数据
        ((DefaultTableModel) tableRunLog.getModel()).setColumnIdentifiers(RUN_LOG_COLUMN_NAMES);
        tableRunLog.getColumnModel().getColumn(0).setPreferredWidth(160);
        tableRunLog.getColumnModel().getColumn(1).setPreferredWidth(320);
        scrollPaneTableRunLog.setViewportView(tableRunLog);
    }

    /**
     * 显示界面
     */
    public void showUI() {
        JFrame frame = new JFrame("游戏广场服务器");
        frame.setContentPane(panelMain);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setSize(960, 480);
        frame.setLocationRelativeTo(null);
        frame.addWindowListener(new WindowAdapter() {
            /**
             * Invoked when a window is in the process of being closed.
             * The close operation can be overridden at this point.
             *
             * @param e
             *  事件
             */
            @Override
            public void windowClosing(WindowEvent e) {  //监听关闭按钮事件
                exitApplication();
            }

        });
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * 更新广场服务器信息
     *
     * @param plazaInfoUpdateEvent plazaInfoUpdateEvent
     */
    @EventListener(PlazaInfoUpdateEvent.class)
    public void plazaInfoUpdate(PlazaInfoUpdateEvent plazaInfoUpdateEvent) {

    }

    /**
     * 更新日志信息
     *
     * @param runLogAddEvent runLogAddEvent
     */
    @EventListener(RunLogAddEvent.class)
    public void runLogAdd(RunLogAddEvent runLogAddEvent) {
        ((DefaultTableModel) tableRunLog.getModel()).addRow(new String[] {

        });
    }

    /**
     * 更新游戏服务器
     *
     * @param gameServerUpdateEvent gameServerUpdateEvent
     */
    @EventListener(GameServerUpdateEvent.class)
    public void gameServerUpdate(GameServerUpdateEvent gameServerUpdateEvent) {
        ((DefaultTableModel) tableGameServer.getModel()).addRow(new String[] {

        });
    }

    /**
     * 退出应用
     */
    private void exitApplication() {
        int type = JOptionPane.showConfirmDialog(panelMain, "确定是否退出?", "提示", JOptionPane.YES_NO_OPTION);
        switch (type) {
            case JOptionPane.YES_OPTION:
                System.exit(0);
                break;
            default:
                break;
        }
    }
}
