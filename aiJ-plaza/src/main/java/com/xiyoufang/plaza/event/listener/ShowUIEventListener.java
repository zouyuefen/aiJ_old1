package com.xiyoufang.plaza.event.listener;

import com.xiyoufang.plaza.event.StartedEvent;
import com.xiyoufang.plaza.ui.MainUIService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Created by 席有芳 on 2018/7/7.
 *
 * @author 席有芳
 */
@Component
public class ShowUIEventListener {

    private final MainUIService mainUIService;

    /**
     *
     */
    private Logger logger = LoggerFactory.getLogger(ShowUIEventListener.class);

    @Autowired
    public ShowUIEventListener(MainUIService mainUIService) {
        this.mainUIService = mainUIService;
    }

    @EventListener(StartedEvent.class)
    public void showUI(StartedEvent startedEvent) {
        mainUIService.start();
    }
}
