package com.xiyoufang;

import com.xiyoufang.common.ApplicationContextProvider;
import com.xiyoufang.server.event.StartedEvent;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * 启动游戏服务
 */
@SpringBootApplication
public class ServerApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(ServerApplication.class).headless(false).web(WebApplicationType.NONE).run(args);
        ApplicationContextProvider.publishEvent(new StartedEvent(""));
    }
}
