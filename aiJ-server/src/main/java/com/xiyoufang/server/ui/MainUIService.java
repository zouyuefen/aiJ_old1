package com.xiyoufang.server.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by 席有芳 on 2018/7/9.
 *
 * @author 席有芳
 */
@Component
public class MainUIService {

    private final MainUI mainUI;

    @Autowired
    public MainUIService(MainUI mainUI) {
        this.mainUI = mainUI;
    }

    /**
     * 开始启动UI
     */
    public void start(){
        mainUI.showUI();
    }
}
