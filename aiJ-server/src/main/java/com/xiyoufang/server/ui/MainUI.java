package com.xiyoufang.server.ui;

import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by 席有芳 on 2018/7/9.
 *
 * @author 席有芳
 */
@Component
public class MainUI {

    private JPanel panelMain;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JTextField textField5;
    private JTextField textField6;
    private JTextField textField7;
    private JButton 启动服务Button;
    private JButton 停止服务Button;
    private JButton 加载房间Button;
    private JButton 创建房间Button;
    private JButton 配置房间Button;
    private JButton 运行详情Button;
    private JButton 开始维护Button;
    private JButton 结束维护Button;

    public void showUI() {
        JFrame frame = new JFrame("游戏服务器");
        frame.setContentPane(panelMain);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setSize(640, 360);
        frame.setLocationRelativeTo(null);
        frame.addWindowListener(new WindowAdapter() {
            /**
             * Invoked when a window is in the process of being closed.
             * The close operation can be overridden at this point.
             *
             * @param e
             *  事件
             */
            @Override
            public void windowClosing(WindowEvent e) {  //监听关闭按钮事件
                exitApplication();
            }

        });
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * 退出应用
     */
    private void exitApplication() {
        int type = JOptionPane.showConfirmDialog(panelMain, "确定是否退出?", "提示", JOptionPane.YES_NO_OPTION);
        switch (type) {
            case JOptionPane.YES_OPTION:
                System.exit(0);
                break;
            default:
                break;
        }
    }
}
