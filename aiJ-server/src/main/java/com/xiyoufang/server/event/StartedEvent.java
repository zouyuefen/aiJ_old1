package com.xiyoufang.server.event;

import org.springframework.context.ApplicationEvent;

/**
 * Created by 席有芳 on 2018/7/9.
 *
 * @author 席有芳
 */
public class StartedEvent extends ApplicationEvent {
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public StartedEvent(Object source) {
        super(source);
    }
}
