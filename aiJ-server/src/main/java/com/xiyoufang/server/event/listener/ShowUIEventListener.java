package com.xiyoufang.server.event.listener;

import com.xiyoufang.server.event.StartedEvent;
import com.xiyoufang.server.ui.MainUIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Created by 席有芳 on 2018/7/9.
 *
 * @author 席有芳
 */
@Component
public class ShowUIEventListener {

    private final MainUIService mainUIService;

    @Autowired
    public ShowUIEventListener(MainUIService mainUIService) {
        this.mainUIService = mainUIService;
    }

    @EventListener(StartedEvent.class)
    public void showUI(StartedEvent startedEvent) {
        mainUIService.start();
    }
}
